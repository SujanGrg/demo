import React from "react";
import {
  View,
  StyleSheet,
  SafeAreaView,
  FlatList,
  Platform,
  StatusBar,
} from "react-native";

import Spacing from "../constants/spacing";
import Colors from "../constants/colors";

import TitleText from "../components/TitleText";

import BusinessHero from "../components/BusinessScreen/BusinessHero";
import ItemCard from "../components/BusinessScreen/ItemCard";

const BusinessScreen = (props) => {
  const itemList = [
    {
      key: 1,
      description: "Pasta Chicken & Bacon Carbonara",
      price: "450",
      image:
        "http://bajekosekuwa.com/Modules/BajekoSekuwa/MenuItemManagement//uploads/f335d7d0-b754-49b5-b85e-e06d8d778325.png",
    },
    {
      key: 2,
      description: "Chicken Chilly Chowmein",
      price: "100",
      image:
        "http://bajekosekuwa.com/Modules/BajekoSekuwa/MenuItemManagement//uploads/f335d7d0-b754-49b5-b85e-e06d8d778325.png",
    },
  ];
  return (
    <SafeAreaView style={styles.scaffold}>
      <View style={styles.container}>
        <BusinessHero {...props} />
        <View style={styles.itemListContainer}>
          <TitleText style={{ fontSize: 18, fontWeight: "500", padding: 8 }}>
            TODAY'S SPECIAL
          </TitleText>
          <FlatList
            style={{ width: "100%" }}
            data={itemList}
            renderItem={(itemData) => {
              return (
                <ItemCard
                  key={itemData.item.key}
                  description={itemData.item.description}
                  price={itemData.item.price}
                />
              );
            }}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  scaffold: {
    flex: 1,
    backgroundColor: Colors.scaffoldBackground,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  container: {
    marginHorizontal: Spacing.main,
    flex: 1,
    backgroundColor: Colors.mainContainer,
    alignItems: "center",
  },
  itemListContainer: {
    flex: 1,
    marginVertical: 12,
    paddingVertical: 8,
    backgroundColor: Colors.hero,
    width: "96%",
    alignItems: "flex-start",
  },
});

export default BusinessScreen;
