import React from "react";
import { Text, StyleSheet } from "react-native";

const DisplayText = (props) => (
  <Text style={{ ...styles.title, ...props.style }}>{props.children}</Text>
);

const styles = StyleSheet.create({
  title: {
    // fontFamily: "open-sans-bold",
    fontSize: 24,
    fontWeight: "700",
    letterSpacing: 2,
  },
});

export default DisplayText;
