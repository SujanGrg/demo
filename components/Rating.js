import * as React from "react";
import { View, StyleSheet } from "react-native";
import { AntDesign } from "@expo/vector-icons";

const Rating = (props) => {
  const filledStars = Math.floor(props.rating / 2);
  const maxStars = Array(5 - filledStars).fill("staro");
  const r = [...Array(filledStars).fill("star"), ...maxStars];

  return (
    <View style={styles.rating}>
      {r.map((type, index) => {
        return (
          <AntDesign key={index} name={type} size={props.size} color="gold" />
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  rating: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginVertical: 4,
  },
});

export default Rating;
