import React from "react";
import { View, StyleSheet, ImageBackground, Image } from "react-native";
import IconButton from "../IconButton";
import { LinearGradient } from "expo-linear-gradient";
import { Ionicons } from "@expo/vector-icons";
import Rating from "../Rating";
import Divider from "../Divider";
import TitleText from "../TitleText";
import BodyText from "../BodyText";
import Colors from "../../constants/colors";

const BusinessHero = (props) => (
  <View style={styles.hero}>
    <View style={styles.appBarAndImageContainer}>
      <ImageBackground
        source={{
          uri: "https://tatomitho.com/img/header.jpg",
        }}
        style={styles.posterImage}
      >
        <View style={styles.appBar}>
          <IconButton
            name="ios-arrow-back"
            style={{ marginLeft: 0 }}
            onPress={() => {
              props.navigation.goBack(null);
            }}
          />
          <View style={styles.rightIcons}>
            <IconButton name="ios-notifications-outline" />
            <IconButton name="ios-location-outline" />
            <IconButton name="ios-qr-code-outline" />
            <IconButton
              name="ios-menu"
              style={{ backgroundColor: "transparent" }}
              size={{ fontSize: 34 }}
              onPress={() => {
                props.navigation.openDrawer();
              }}
            />
          </View>
        </View>
        <View style={styles.titleContainer}>
          <LinearGradient
            colors={[
              "rgba(52, 52, 52, 0.0)",
              "rgba(52, 52, 52, 0.2)",
              "rgba(52, 52, 52, 0.5)",
            ]}
            style={{
              flexDirection: "row",
              alignItems: "flex-end",
              width: "100%",
              overflow: "visible",
            }}
          >
            <Image
              style={styles.image}
              source={{
                uri: "https://tatomitho.com/img/header.jpg",
              }}
            ></Image>
            <View style={styles.title}>
              <TitleText style={{ color: "white", fontSize: 20 }}>
                Baje Ko Sekuwa
              </TitleText>
              <BodyText style={{ color: "white" }}>
                Kathmandu, jaulakhel
              </BodyText>
            </View>
          </LinearGradient>
        </View>
      </ImageBackground>
    </View>
    <View style={styles.ratingContainer}>
      <BodyText style={{ fontSize: 16 }} numberOfLines={1}>
        Italian, FastFood, TakeOut, Delivery
      </BodyText>
      <View
        style={{
          width: "30%",
          alignItems: "flex-start",
        }}
      >
        <Rating rating="5" size={16} />
      </View>
    </View>
    <View style={styles.distanceContainer}>
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <BodyText style={{ fontSize: 16 }}>7.5 km away</BodyText>
        <Divider
          style={{ width: 20, marginHorizontal: 4 }}
          color={{ backgroundColor: "black" }}
        ></Divider>
        <Ionicons name="ios-location-outline" style={{ fontSize: 16 }} />
      </View>
      <View style={{ width: "30%", alignItems: "flex-start" }}>
        <BodyText style={{ fontSize: 16 }}>$$$</BodyText>
      </View>
    </View>
    <View style={styles.buttonsContainer}>
      <View style={styles.buttons}>
        <IconButton
          name="ios-call-outline"
          style={{
            ...styles.individualButton,
            height: 50,
            width: 50,
          }}
        />
        <IconButton
          name="ios-book-outline"
          style={{
            ...styles.individualButton,
            height: 50,
            width: 50,
          }}
        />
        <IconButton
          name="arrow-redo-outline"
          style={{
            ...styles.individualButton,
            height: 50,
            width: 50,
          }}
        />
        <IconButton
          name="ios-ellipsis-horizontal"
          style={{
            ...styles.individualButton,
            height: 50,
            width: 50,
          }}
        />
      </View>
    </View>
  </View>
);

const styles = StyleSheet.create({
  appBar: {
    width: "100%",
    padding: 8,
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  rightIcons: {
    flexDirection: "row",
  },
  hero: { height: 360, backgroundColor: "white", width: "100%" },
  appBarAndImageContainer: {
    height: "58%",
    width: "100%",
  },
  posterImage: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "space-between",
    alignItems: "center",
  },
  titleContainer: {
    // backgroundColor: "red",
    width: "100%",
  },
  title: {
    marginLeft: 132,
    marginVertical: 8,
  },
  image: {
    width: 80,
    height: 80,
    marginLeft: 24,
    position: "absolute",
    bottom: -20,
    borderColor: Colors.cardBackground,
    borderWidth: 4,
  },
  ratingContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 24,
    marginHorizontal: 8,
  },
  distanceContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 4,
    marginHorizontal: 8,
  },
  buttonsContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  buttons: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginBottom: 8,
    marginHorizontal: 8,
  },

  individualButton: {
    marginHorizontal: 8,
    backgroundColor: "white",
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.26,
    elevation: 4,
  },
});

export default BusinessHero;
