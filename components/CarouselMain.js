import * as React from "react";
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Animated,
  Platform,
  ImageBackground,
  TouchableOpacity,
  TouchableNativeFeedback,
} from "react-native";
const { width } = Dimensions.get("window");
import { getMovies } from "../provider/api";
import MainButton from "./MainButton";
import Rating from "./Rating";
import Colors from "../constants/colors";
import BodyText from "../components/BodyText";
import TitleText from "../components/TitleText";
import { Ionicons } from "@expo/vector-icons";

const SPACING = 10;
// const ITEM_SIZE = Platform.OS === "ios" ? width * 0.72 : width * 0.74;
const ITEM_SIZE = Platform.OS === "ios" ? width * 0.62 : width * 0.66;
const EMPTY_ITEM_SIZE = (width - ITEM_SIZE) / 2;

const Loading = () => (
  <View style={styles.loadingContainer}>
    <Text style={styles.paragraph}>Loading...</Text>
  </View>
);

const CarouselMain = (props) => {
  let TouchableCmp = TouchableOpacity;
  if (Platform.OS === "android" && Platform.Version >= 21) {
    TouchableCmp = TouchableNativeFeedback;
  }

  const [movies, setMovies] = React.useState([]);
  const scrollX = React.useRef(new Animated.Value(0)).current;
  React.useEffect(() => {
    const fetchData = async () => {
      const movies = await getMovies();
      setMovies([{ key: "empty-left" }, ...movies, { key: "empty-right" }]);
    };

    if (movies.length === 0) {
      fetchData(movies);
    }
  }, [movies]);

  if (movies.length === 0) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <Animated.FlatList
        showsHorizontalScrollIndicator={false}
        data={movies}
        keyExtractor={(item) => item.key}
        horizontal
        bounces={false}
        decelerationRate={Platform.OS === "ios" ? 0 : 0.98}
        renderToHardwareTextureAndroid
        contentContainerStyle={{ alignItems: "flex-start" }}
        snapToInterval={ITEM_SIZE}
        snapToAlignment="start"
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false }
        )}
        scrollEventThrottle={16}
        renderItem={({ item, index }) => {
          if (!item.poster) {
            return <View style={{ width: EMPTY_ITEM_SIZE }} />;
          }
          const inputRange = [
            (index - 2) * ITEM_SIZE,
            (index - 1) * ITEM_SIZE,
            index * ITEM_SIZE,
          ];
          const translateY = scrollX.interpolate({
            inputRange,
            outputRange: [-10, 0, -10],
            extrapolate: "clamp",
          });

          return (
            <View
              style={{
                width: ITEM_SIZE,
              }}
            >
              <TouchableCmp
                style={{ flex: 1 }}
                onPress={() => {
                  props.navigation.navigate("Business");
                }}
              >
                <Animated.View
                  style={{
                    height: 180,
                    marginHorizontal: SPACING,
                    marginTop: 14,
                    marginBottom: 18,
                    overflow: "hidden",
                    alignItems: "flex-start",
                    transform: [{ translateY }],
                    borderTopEndRadius: 34,
                    borderTopStartRadius: 34,
                    borderBottomStartRadius: 34,
                    backgroundColor: Colors.cardBackground,
                    shadowColor: "black",
                    shadowOffset: { width: 0, height: 2 },
                    shadowRadius: 6,
                    shadowOpacity: 0.26,
                    elevation: 6,
                  }}
                >
                  <View style={styles.posterContainer}>
                    <ImageBackground
                      source={{ uri: item.poster }}
                      style={styles.posterImage}
                    >
                      <View style={styles.buttonsParentContainer}>
                        <View style={styles.buttonsContainer}>
                          <MainButton
                            name="open"
                            style={{ backgroundColor: Colors.buttonPrimary }}
                          />
                          <MainButton
                            name="visited"
                            style={{
                              backgroundColor: Colors.buttonSecondary,
                            }}
                          />
                        </View>
                        <Ionicons
                          name="ellipsis-horizontal-sharp"
                          style={styles.icon}
                        />
                      </View>
                      <View style={styles.ratingContainer}>
                        <MainButton
                          name={item.type}
                          style={{ backgroundColor: Colors.buttonTertiary }}
                          buttonText={{ color: "black" }}
                        />
                        <BodyText style={{ color: "white", fontWeight: "500" }}>
                          {item.distance}
                        </BodyText>
                        <Rating rating={item.rating} />
                      </View>
                    </ImageBackground>
                  </View>
                  <View style={styles.contentContainer}>
                    <TitleText numberOfLines={1} style={{ marginBottom: 2 }}>
                      {item.title}
                    </TitleText>
                    <BodyText numberOfLines={2}>{item.description}</BodyText>
                  </View>
                </Animated.View>
              </TouchableCmp>
            </View>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  container: {
    // backgroundColor: "red",
    height: 208,
    alignItems: "center",
  },

  posterContainer: {
    height: "62%",
    width: "100%",
  },
  posterImage: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "space-between",
  },
  buttonsParentContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingTop: 12,
    paddingHorizontal: 16,
  },
  buttonsContainer: {
    flexDirection: "row",
  },
  ratingContainer: {
    paddingVertical: 4,
    paddingHorizontal: 16,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.8)",
  },
  contentContainer: {
    paddingHorizontal: 16,
    paddingVertical: 4,
  },
  icon: {
    color: "white",
    fontSize: 20,
  },
});

export default CarouselMain;
