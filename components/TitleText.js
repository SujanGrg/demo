import React from "react";
import { Text, StyleSheet } from "react-native";

const TitleText = (props) => (
  <Text {...props} style={{ ...styles.title, ...props.style }}>
    {props.children}
  </Text>
);

const styles = StyleSheet.create({
  title: {
    // fontFamily: "open-sans-bold",
    fontSize: 14,
    fontWeight: "700",
  },
});

export default TitleText;
