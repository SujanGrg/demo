import React, { useState } from "react";
import {
  View,
  StyleSheet,
  SafeAreaView,
  Text,
  ScrollView,
  Platform,
  StatusBar,
} from "react-native";
import IconButton from "../components/IconButton";
import Spacing from "../constants/spacing";
import Colors from "../constants/colors";
import DisplayText from "../components/DisplayText";
import HeroTextInput from "../components/HeroTextInput";
import HeroRadioInput from "../components/HeroRadioInput";
import CarouselMain from "../components/CarouselMain";
import LineLable from "../components/LineLable";
import NearByList from "../components/NearByList";
import Divider from "../components/Divider";
import HighLight from "../components/Highlight";

const HomeScreen = (props) => {
  const [enteredValue, setEnteredValue] = useState("");

  return (
    <SafeAreaView style={styles.scaffold}>
      <View style={styles.container}>
        <View style={styles.appBar}>
          <IconButton name="ios-notifications-outline" />
          <IconButton name="ios-location-outline" />
          <IconButton name="ios-qr-code-outline" />
          <IconButton
            name="ios-menu"
            style={{ backgroundColor: "transparent" }}
            size={{ fontSize: 34 }}
            onPress={() => {
              props.navigation.openDrawer();
            }}
          />
        </View>
        <ScrollView
          contentContainerStyle={{ alignItems: "center", width: "100%" }}
        >
          <View style={styles.hero}>
            <DisplayText>BRAND</DisplayText>
            <View style={styles.heroInputContainer}>
              <HeroTextInput
                label="Search"
                iconName="ios-search-sharp"
                placeholder="Resturanr, Hotels, Resorts"
                containerStyle={{ width: "56%" }}
              />
              <HeroTextInput
                label="In"
                iconName="ios-location-sharp"
                placeholder="Address"
                containerStyle={{ width: "40%" }}
              />
            </View>
            <View style={styles.heroRadioButtonsContainer}>
              <HeroRadioInput />
            </View>
          </View>
          <Divider />
          <Text style={styles.feedback}>
            <HighLight>Resturants </HighLight>and
            <HighLight> Resorts </HighLight>
            from <HighLight> Freak Street, Kathmandu </HighLight>2 for keyword
            <HighLight> 'sekuwa'</HighLight>
          </Text>

          <View style={styles.carousel}>
            <CarouselMain {...props} />
          </View>
          <LineLable name="nearby" />
          <NearByList />
          <LineLable name="favs" />
          <NearByList />
          <LineLable name="visited" />
          <NearByList />
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  scaffold: {
    flex: 1,
    backgroundColor: Colors.scaffoldBackground,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  container: {
    marginHorizontal: Spacing.main,
    flex: 1,
    backgroundColor: Colors.mainContainer,
    alignItems: "center",
  },
  appBar: {
    width: "100%",
    padding: 8,
    backgroundColor: Colors.primary,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  hero: {
    marginVertical: 12,
    paddingVertical: 8,
    backgroundColor: Colors.hero,
    width: "96%",
    alignItems: "center",
  },
  heroInputContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 8,
    marginTop: 6,
  },
  heroRadioButtonsContainer: {
    height: 30,
  },
  feedback: {
    textAlign: "center",
    fontSize: 10,
    marginTop: 6,
  },
  carousel: { marginTop: 16 },
});

export default HomeScreen;
