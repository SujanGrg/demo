export default {
  primary: "#4a4a4b",
  buttonBackground: "#cccccc",
  scaffoldBackground: "#d4d4d4",
  cardBackground: "#e1e1e1",
  mainContainer: "#eeeeee",
  tab: "#bbbbbb",
  hero: "#e4e4e4",
  label: "#eeeeee",
  buttonPrimary: "#0b9f35",
  buttonSecondary: "#1c94ea",
  buttonTertiary: "#eff6f5",
  buttonFourth: "#cccccc",
  lineLable: "#939090",
};
