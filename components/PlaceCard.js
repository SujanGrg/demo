import React from "react";
import {
  Text,
  StyleSheet,
  View,
  ImageBackground,
  Dimensions,
} from "react-native";
import Colors from "../constants/colors";
import TitleText from "./TitleText";
import Rating from "./Rating";
import MainButton from "./MainButton";
import BodyText from "./BodyText";
import { Ionicons } from "@expo/vector-icons";

const { width } = Dimensions.get("window");
const PlaceCard = (props) => (
  <View style={{ ...styles.container, ...props.style }}>
    <View style={{ ...styles.imageContainer }}>
      <ImageBackground
        source={{ uri: "https://tatomitho.com/img/header.jpg" }}
        style={styles.posterImage}
      ></ImageBackground>
    </View>
    <View style={{ ...styles.contentContainer }}>
      <TitleText numberOfLines={1} style={{ fontWeight: "500", fontSize: 12 }}>
        Hotel Mysteic Mountain
      </TitleText>
      <Rating rating="5" />
      <View style={{ ...styles.typeContainer }}>
        <MainButton
          name="resort"
          style={{ backgroundColor: Colors.buttonTertiary }}
          buttonText={{ color: "black" }}
        />
        <Ionicons name="car" style={{ fontSize: 16, marginLeft: 8 }} />
        <BodyText style={{ fontSize: 12 }}>10 km</BodyText>
      </View>
      <View style={{ ...styles.statusContainer }}>
        <MainButton
          name="closed"
          style={{ backgroundColor: Colors.buttonFourth }}
          buttonText={{ color: "black" }}
        />
        <Ionicons name="ellipsis-horizontal-sharp" style={{ fontSize: 20 }} />
      </View>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    height: 112,
    width: width * 0.62,
    backgroundColor: Colors.cardBackground,
    borderRadius: 24,
    overflow: "hidden",
    marginHorizontal: 8,
    backgroundColor: Colors.cardBackground,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.26,
    elevation: 6,
    marginVertical: 12,
  },
  imageContainer: {
    height: "100%",
    width: "30%",
  },
  posterImage: {
    flex: 1,
    resizeMode: "cover",
  },
  contentContainer: {
    flex: 1,
    paddingVertical: 8,
    paddingHorizontal: 16,
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
  typeContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  statusContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
    width: "100%",
  },
});

export default PlaceCard;
