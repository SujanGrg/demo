import React from "react";
import { View } from "react-native";
import Colors from "../constants/colors";

const Divider = (props) => (
  <View style={{ flexDirection: "row", alignItems: "center", ...props.style }}>
    <View
      style={{
        flex: 1,
        height: 2,
        backgroundColor: Colors.scaffoldBackground,
        ...props.color,
      }}
    />
  </View>
);

export default Divider;
