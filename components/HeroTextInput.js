import React from "react";
import { View, TextInput, StyleSheet, Text } from "react-native";
import Colors from "../constants/colors";
import { Ionicons } from "@expo/vector-icons";

const HeroTextInput = (props) => {
  return (
    <View style={{ ...styles.container, ...props.containerStyle }}>
      <View style={styles.labelContainer}>
        <Text>{props.label}</Text>
      </View>
      <TextInput {...props} style={{ ...styles.input, ...props.style }} />
      <Ionicons name={props.iconName} style={styles.icon} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 8,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.26,
    elevation: 8,
    overflow: "hidden",
  },
  labelContainer: {
    backgroundColor: Colors.label,
    height: "100%",
    paddingHorizontal: 8,
    justifyContent: "center",
    height: 34,
  },
  input: {
    flex: 1,
    paddingHorizontal: 8,
    height: 34,
  },
  icon: {
    fontSize: 16,
    paddingHorizontal: 8,
  },
});

export default HeroTextInput;
