import React from "react";
import { StyleSheet, FlatList } from "react-native";
import PlaceCard from "./PlaceCard";

const data = [1, 2, 3, 4];
const NearByList = (props) => (
  <FlatList
    horizontal
    showsHorizontalScrollIndicator={false}
    style={styles.container}
    data={data}
    renderItem={(itemData) => {
      return <PlaceCard />;
    }}
  />
);

const styles = StyleSheet.create({
  container: {
    flexGrow: 0,
  },
});

export default NearByList;
