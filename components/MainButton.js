import * as React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

export default function MainButton(props) {
  return (
    <TouchableOpacity activeOpacity={0.6} onPress={props.onPress}>
      <View style={{ ...styles.button, ...props.style }}>
        <Text style={{ ...styles.buttonText, ...props.buttonText }}>
          {props.name}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: "red",
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 6,
    marginRight: 4,
    justifyContent: "center",
    marginRight: 6,
  },
  buttonText: {
    opacity: 0.9,
    fontSize: 12,
    textAlign: "center",
    color: "white",
    fontWeight: "600",
  },
});
