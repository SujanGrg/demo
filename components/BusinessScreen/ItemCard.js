import React from "react";
import { View, StyleSheet, Image } from "react-native";
import IconButton from "../IconButton";

import BodyText from "../BodyText";
import Colors from "../../constants/colors";

const ItemCard = (props) => (
  <View
    style={{
      flexDirection: "row",
      justifyContent: "flex-start",
      marginVertical: 8,
      marginHorizontal: 8,
      borderRadius: 16,
      overflow: "hidden",
      alignItems: "center",
    }}
  >
    <View
      style={{
        height: 80,
        width: "30%",
        borderRadius: 16,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 4,
        overflow: "hidden",
      }}
    >
      <Image
        style={{ flex: 1, resizeMode: "cover" }}
        source={{
          uri: "https://tatomitho.com/img/header.jpg",
        }}
      ></Image>
    </View>
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
        width: "70%",
      }}
    >
      <View
        style={{
          flexDirection: "row",
          marginVertical: 8,
          backgroundColor: Colors.cardBackground,
          alignItems: "center",
          shadowColor: "black",
          shadowOffset: { width: 0, height: 2 },
          shadowRadius: 6,
          shadowOpacity: 0.26,
          elevation: 4,
          borderRadius: 16,
        }}
      >
        <View
          style={{
            padding: 8,
            justifyContent: "space-around",
            maxWidth: "72%",
          }}
        >
          <BodyText>{props.description}</BodyText>
          <BodyText style={{ marginTop: 8 }}>{props.price}</BodyText>
        </View>

        <View
          style={{
            height: "100%",
            width: 2,
            backgroundColor: Colors.hero,
          }}
        >
          <View style={{ flex: 1 }}></View>
        </View>

        <IconButton
          name="ios-add-sharp"
          style={{
            ...styles.individualButton,
            height: 30,
            width: 30,
          }}
        />
      </View>
    </View>
  </View>
);

const styles = StyleSheet.create({
  individualButton: {
    marginHorizontal: 8,
    backgroundColor: "white",
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.26,
    elevation: 4,
  },
});

export default ItemCard;
