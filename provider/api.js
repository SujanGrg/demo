// const API_URL ="";

export const getMovies = async () => {
  // const { results } = await fetch(API_URL).then((x) => x.json());
  const results = [
    {
      id: 1,
      title: "Tato Mitho",
      poster: "https://tatomitho.com/img/header.jpg",
      rating: 6,
      description:
        "TatoMitho is all about providing you with the best, delicious and hot food. Be it collection or delivery, we got you covered whenever, wherever you want your food.",
      distance: "10 km",
      type: "hotel",
    },
    {
      id: 2,
      title: "Baje Ko Sekuwa",
      poster:
        "http://www.bajekosekuwa.com/Modules/BajekoSekuwa/MenuItemManagement//uploads/4822749d-8161-45ce-abbe-295088d27660.JPG",
      rating: 8,
      description:
        "A pioneer in Nepali food industry, Bajeko Sekuwa is an authentic Nepali restaurant with rich culinary history stretching over four decades. Enriching the values and taste of Nepal, Bajeko Sekuwa serves you with the most authentic non-vegetarian as well as vegetarian items.",
      distance: "5 km",
      type: "resturant",
    },

    {
      id: 3,
      title: "Pizmo",
      poster:
        "https://scontent.fpkr1-1.fna.fbcdn.net/v/t1.6435-9/157325816_2987326784837469_4473005241610363221_n.jpg?_nc_cat=110&ccb=1-3&_nc_sid=e3f864&_nc_ohc=YoUOvlTO5eIAX_HqZmd&_nc_ht=scontent.fpkr1-1.fna&oh=f4ed76a83c87f52024df48e3c2b5ae1a&oe=6093DE22",
      rating: 10,
      description:
        "A place to have the Best Wraps and Special Burgers in Town with amazing Pizzas, Pizmo TexWraps, Mexicana Wraps, Honey BBQ Wraps and Burgers are nowhere to be found in entire Nepal.",
      distance: "1 km",
      type: "resturant",
    },
  ];
  const movies = results.map((item) => ({
    key: item.id,
    title: item.title,
    poster: item.poster,
    rating: item.rating,
    description: item.description,
    distance: item.distance,
    type: item.type,
  }));

  return movies;
};
