import React from "react";
import { Text, StyleSheet } from "react-native";

const HighLight = (props) => (
  <Text {...props} style={{ ...styles.body, ...props.style }}>
    {props.children}
  </Text>
);

const styles = StyleSheet.create({
  body: {
    // fontFamily: "open-sans",
    fontWeight: "700",
  },
});

export default HighLight;
