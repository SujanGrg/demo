import React from "react";
import { StyleSheet, View } from "react-native";
import TitleText from "./TitleText";
import Colors from "../constants/colors";

const BodyText = (props) => (
  <View style={styles.lineLable}>
    <View style={{ ...styles.divider, ...{ width: "16%" } }}></View>
    <TitleText style={{ color: Colors.lineLable, paddingHorizontal: 4 }}>
      {props.name}
    </TitleText>
    <View style={{ ...styles.divider, ...{ flex: 1 } }}></View>
  </View>
);

const styles = StyleSheet.create({
  divider: {
    height: 2,
    width: "100%",
    backgroundColor: Colors.scaffoldBackground,
  },

  lineLable: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export default BodyText;
