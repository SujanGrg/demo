import React from "react";
import DemoNavigator from "./navigation/DemoNavigator";

export default function App() {
  return <DemoNavigator />;
}
