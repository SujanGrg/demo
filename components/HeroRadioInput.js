import React from "react";
import RadioButton from "react-native-customizable-radio-button";
import { View, TextInput, StyleSheet, Text } from "react-native";
import Colors from "../constants/colors";
import { Ionicons } from "@expo/vector-icons";

const HeroRadioInput = (props) => {
  const options = [
    {
      id: 1,
      text: "Hotel",
    },
    {
      id: 2,
      text: "Resort",
    },

    {
      id: 3,
      text: "Resturant",
    },
  ];

  onValueChange = (item) => {
    console.log(item);
  };

  return (
    <RadioButton
      data={options}
      onValueChange={() => {}}
      formStyle={styles.form}
      containerStyle={styles.container}
      circleContainerStyle={{ height: 16, width: 16 }}
      innerCircleStyle={{ height: 6, width: 6 }}
      labelStyle={{ fontSize: 12 }}
    />
  );
};

const styles = StyleSheet.create({
  form: {
    flexDirection: "row",
    width: "80%",
    justifyContent: "space-around",
  },
  container: {
    height: "100%",
  },
});

export default HeroRadioInput;
