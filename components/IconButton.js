import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/colors";

const IconButton = (props) => {
  return (
    <TouchableOpacity activeOpacity={0.6} onPress={props.onPress}>
      <View style={{ ...styles.button, ...props.style }}>
        <Ionicons name={props.name} style={{ ...styles.icon, ...props.size }} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: Colors.buttonBackground,
    height: 40,
    width: 40,
    borderRadius: 40,
    justifyContent: "center",
    alignContent: "center",
    marginLeft: 16,
  },
  icon: {
    fontSize: 24,
    textAlign: "center",
  },
});

export default IconButton;
