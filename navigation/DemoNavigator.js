import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Ionicons } from "@expo/vector-icons";
import { View } from "react-native";

import HomeScreen from "../screens/HomeScreen";
import BusinessScreen from "../screens/BusinessScreen";
import ProfileScreen from "../screens/ProfileScreen";
import SearchScreen from "../screens/SearchScreen";

import Colors from "../constants/colors";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const DashBoardNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Business" component={BusinessScreen} />
    </Stack.Navigator>
  );
};

const BottomTabNavigator = () => {
  const customTabBarStyle = {
    activeTintColor: "black",
    inactiveTintColor: "grey",
    style: { backgroundColor: Colors.tab },
  };
  return (
    <Tab.Navigator tabBarOptions={customTabBarStyle}>
      <Tab.Screen
        name="DashBoard"
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color }) => (
            <Ionicons name="ios-home" color={color} size={26} />
          ),
        }}
        component={DashBoardNavigator}
      />
      <Tab.Screen
        name="Search"
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color }) => (
            <View
              style={{
                position: "absolute",
                bottom: 0, // space from bottombar
                height: 60,
                width: 60,
                backgroundColor: Colors.buttonBackground,
                borderRadius: 30,
                justifyContent: "center",
                alignItems: "center",
                elevation: 1,
              }}
            >
              <Ionicons name="ios-search-sharp" color={color} size={34} />
            </View>
          ),
        }}
        component={SearchScreen}
      />
      <Tab.Screen
        name="Profile"
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color }) => (
            <Ionicons name="ios-person-circle" color={color} size={26} />
          ),
        }}
        component={ProfileScreen}
      />
    </Tab.Navigator>
  );
};

const DemoNavigator = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Dashboard" drawerPosition="right">
        <Drawer.Screen name="Dashboard" component={BottomTabNavigator} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default DemoNavigator;
